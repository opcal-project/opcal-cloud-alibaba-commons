# opcal-cloud-alibaba-commons is no longer actively maintained, disappointed with sca development roadmap.

[![pipeline status](https://gitlab.com/opcal-project/opcal-cloud-alibaba-commons/badges/main/pipeline.svg)](https://gitlab.com/opcal-project/opcal-cloud-alibaba-commons/-/commits/main)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=opcal-project_opcal-cloud-alibaba-commons&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=opcal-project_opcal-cloud-alibaba-commons)
[![codecov](https://codecov.io/gl/opcal-project/opcal-cloud-alibaba-commons/branch/\x6d61696e/graph/badge.svg?token=Q8Z9IMXZXZ)](https://codecov.io/gl/opcal-project/opcal-cloud-alibaba-commons)


## Release Train Table
|  Release  |   Branch  | Spring Boot | Spring Cloud | Spring Cloud Alibaba version |
|   :---:   |   :---:   |    :---:    |     :---:    |         :---:                |
|  3.2.1.0  |    main   |   3.2.1     |    2023.0.0  |       2023.0.0.0             |
|  3.1.7.0  |    3.1.x  |   3.1.7     |    2022.0.4  |       2022.0.0.0             |
|  3.0.13.1 |    3.0.x  |   3.0.13    |    2022.0.4  |       2022.0.0.0             |
|  2.7.18.1 |    2.7.x  |   2.7.18    |    2021.0.9  |       2021.0.5.0             |



## How to Use
### Add maven parent

Replace {opcal-cloud-alibaba.version} that if necessary to support required Spring Boot, Spring Cloud and Spring Cloud Alibaba version.
```xml
<parent>
    <groupId>xyz.opcal.cloud</groupId>
    <artifactId>opcal-cloud-alibaba-starter-parent</artifactId>
    <version>{opcal-cloud-alibaba.version}</version>
    <relativePath/>
</parent>
```
package xyz.opcal.cloud.commons.integration.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import xyz.opcal.cloud.commons.web.WebConstants;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class HttpbinControllerTests {

	public static final String HTTPBIN_API = "/httpbin/get";

	private @Autowired TestRestTemplate testRestTemplate;

	@Test
	void httpbin() {
		HttpHeaders headers = new HttpHeaders();
		headers.set(WebConstants.HEADER_X_REQUEST_ID, String.valueOf(System.currentTimeMillis()));
		headers.set(WebConstants.HEADER_X_REAL_IP, "10.0.0.4");
		ResponseEntity<String> result = testRestTemplate.exchange(HTTPBIN_API, HttpMethod.GET, new HttpEntity<>(headers), String.class);
		assertEquals(HttpStatus.OK, result.getStatusCode());
	}
}